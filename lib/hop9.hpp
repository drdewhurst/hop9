/**
 * @file hop9.hpp
 * @author David Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.1
 * @date 2023-01-12
 * 
 * @copyright Copyright (c) 2023 - present.
 * 
 */

#ifndef HOP9_H
#define HOP9_H

#ifdef HOP9_VERBOSE
#include <iostream>
#endif

#include <deque>
#include <functional>
#include <optional>
#include <ranges>
#include <unordered_map>
#include <vector>

/**
 * @brief Parameters interact with state variables.
 * 
 * @tparam State The type of state with which the parameters interact
 */
template<typename State>
struct Parameters {
    virtual std::string repr() = 0;
};

/**
 * @brief A unique agent type
 * 
 * @tparam State The type of state with which the agent interacts
 */
template<typename State>
struct Agent {};

/**
 * @brief Information consists of state and parameters.
 * 
 * @tparam State 
 */
template<typename State>
struct Information {
    State state;
    Parameters<State> params;

    std::string repr() {
        std::string s = "Information {\n";
        s += "\tstate = " + state.repr() + ", \n";
        s += "\tparams = " + params.repr() + "\n}";
        return s;
    }
};

/**
 * @brief A description of what is to be done and a current view of state and parameters
 * 
 * @tparam State 
 */
template<typename State>
struct Task {
    std::string name;
    Information<State> info;

    std::string repr() {
        return "Task {\n\tname = " + name + ",\n\tinfo = " + info.repr() + "\n}";
    }
};

/**
 * @brief An operator modifies state
 * 
 * @tparam State 
 */
template<typename State>
using hop_operator = std::function<std::optional<Information<State>>(Task<State>)>;

template<typename State>
using task_queue = std::deque<Task<State>>;

/**
 * @brief Displays a human-readable representation of tasks to be completed
 * 
 * @tparam State 
 * @param q a task queue
 * @return std::string 
 */
template<typename State>
std::string display(task_queue<State>& q) {
    std::string s = "task queue = { ";
    if (q.size() > 0) {
        for (auto& t : q) s += t.name + ", ";
    }
    s += " }";
    return s;
}

/**
 * @brief A method decomposes a planning problem into subproblems to be solved sequentially
 * 
 * @tparam State 
 */
template<typename State>
using hop_method = std::function<std::optional<task_queue<State>>(Task<State>)>;

/**
 * @brief A Universe contains all operators and methods that can be used to solve a planning problem.
 * 
 * @tparam State 
 */
template<typename State>
struct Universe {

    std::unordered_map<std::string, hop_operator<State>> operators;
    std::unordered_map<std::string, std::vector<hop_method<State>>> methods;

    /**
     * @brief Adds an operator to the universe
     * 
     * @param name
     * @param op
     * 
     */
    void add_operator(std::string name, hop_operator<State> op) {
        operators.insert_or_assign(name, op);
    }

    /**
     * @brief Adds a method to the universe
     * 
     * @param name 
     * @param method 
     */
    void add_method(std::string name, hop_method<State> method) {
        if (!methods.contains(name)) {
            std::vector<hop_method<State>> x{method};
            methods.insert_or_assign(name, x);
        } else {
            methods[name].push_back(method);
        }
    }

    /**
     * @brief Adds multiple methods to the universe
     * 
     * @param name 
     * @param methods_ 
     */
    void add_methods(std::string name, std::vector<hop_method<State>> methods_) {
        for (auto& m : methods_) add_method(name, m);
    }

    /**
     * @brief Attempts to solve the remainder of the planning problem by searching for an appropriate operator.
     * 
     * If an operator is found, the state is updated by applying the operator to state and then the remainder of
     * the planning problem is solved recursively. If the planning problem is unable to be solved, or if no operator
     * is found, returns null.
     * 
     * @param tasks the tasks to be completed
     * @param partial_plan the accumulated plan thus far
     * @param the_task the current task to be completed
     * @param depth the recursion depth
     * @return std::optional<task_queue<State>> 
     */
    std::optional<task_queue<State>> search_operators(
        task_queue<State> tasks,
        task_queue<State> partial_plan,
        Task<State> the_task,
        size_t depth
    ) {

        #ifdef HOP9_VERBOSE
        std::cout << "\n~~~\nsearch_operators: depth = " << depth << ", task = " << the_task.repr() << std::endl;
        #endif

        hop_operator<State> op = operators[the_task.name];
        auto new_info = op(the_task);
        if (new_info.has_value()) {

            #ifdef HOP9_VERBOSE
            std::cout << "search_operators: new_info = " << new_info.value().repr() << std::endl;
            #endif

            tasks.pop_front();

            #ifdef HOP9_VERBOSE
            std::cout << "search_operators: " << display(tasks) << std::endl;
            #endif

            // update the information set at all other tasks to be completed
            for (auto& t : tasks) t.info = new_info.value();
            partial_plan.push_back(the_task);
            return plan(tasks, partial_plan, depth + 1);
        } else {
            return std::nullopt;
        }
    }

    /**
     * @brief Attempts to solve the remainder of the planning problem by searching for an appropriate method.
     * 
     * For each candidate method, uses the method to find a further collection of subtasks to complete. The first such
     * collection of subtasks is added to the front of the tasks to be completed, and the remainder of the planning 
     * problem is solved recursively. If no such collection of subtasks exists for *any* candidate
     * method, returns null.
     * 
     * @param tasks the tasks to be completed
     * @param partial_plan the accumulated plan thus far
     * @param the_task the current task to be completed
     * @param depth the recursion depth
     * @return std::optional<task_queue<State>> 
     */
    std::optional<task_queue<State>> search_methods(
        task_queue<State> tasks,
        task_queue<State> partial_plan,
        Task<State> the_task,
        size_t depth
    ) {
        #ifdef HOP9_VERBOSE
        std::cout << "\n~~~\nsearch_methods: depth = " << depth << ", task = " << the_task.repr() << std::endl;
        #endif

        auto& candidates = methods[the_task.name];
        for (hop_method<State> method : candidates) {
            auto subtasks = method(the_task);

            #ifdef HOP9_VERBOSE
            if (subtasks.has_value()) {
                std::cout << "search_methods: " << display(subtasks.value()) << std::endl;
            } else {
                std::cout << "search_methods: backtracking on current method" << std::endl;
            }
            #endif

            if (subtasks.has_value()) {
                // new_tasks = [a, b], subtasks = [c, d, e]
                // just completed a!
                // new_tasks = [c, d, e, b]
                task_queue<State> new_tasks;
                for (size_t ix = 1; ix != tasks.size(); ix++) new_tasks.push_back(tasks[ix]);
                for (auto& t : std::ranges::views::reverse(subtasks.value())) new_tasks.push_front(t);
                auto new_solution = plan(new_tasks, partial_plan, depth + 1);
                if (new_solution.has_value()) return new_solution;
            }
        }
        return std::nullopt;
    }

    /**
     * @brief Solves a hierarchical task network (HTN) planning problem via backtracking.
     * 
     * @param tasks the tasks to be completed
     * @param partial_plan the accumulated plan thus far
     * @param depth the recursion depth 
     * @return std::optional<task_queue<State>> 
     */
    std::optional<task_queue<State>> plan(
        task_queue<State> tasks, 
        task_queue<State> partial_plan = {}, 
        size_t depth = 0
    ) {

        #ifdef HOP9_VERBOSE
        std::cout << "plan: " << display(tasks) << std::endl;
        #endif

        if (tasks.size() == 0) return partial_plan;
        auto the_task = tasks.front();
        if (operators.contains(the_task.name)) {
            return search_operators(tasks, partial_plan, the_task, depth);
        } else if (methods.contains(the_task.name)) {
            return search_methods(tasks, partial_plan, the_task, depth);
        } else {
            return std::nullopt;
        }

    }

};


#endif  // HOP9_H