/**
 * @file test_basic.cpp
 * @author David Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.1
 * @date 2023-01-12
 * 
 * @copyright Copyright (c) 2023 - present
 * 
 */

#ifndef HOP9_VERBOSE
#define HOP9_VERBOSE 1;
#endif

#include <hop9.hpp>

#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>


struct travel_state {
    std::unordered_map<std::string, std::string> loc;
    double owe;
    std::unordered_map<std::string, std::unordered_map<std::string, double>> dist;
    double cash;

    std::string repr() {
        std::string s = "travel_state {\n\tloc = {";
        for (auto& [k, v] : loc) {
            s += k + ": " + v + ", ";
        }
        s += "}, \n\towe = " + std::to_string(owe) + ", ";
        s += "\n\tdist = {";
        for (auto& [orig, dest] : dist) {
            for (auto& [d, distance] : dest) {
                s += "{orig = " + orig + ", dest = " + d + ", distance = " + std::to_string(distance) + "}, ";
            }
        }
        s += "},\n\tcash = " + std::to_string(cash) + "}";
        return s;
    }
};

template<>
struct Agent<travel_state> {
    std::string name;
};

template<>
struct Parameters<travel_state> {
    std::string x;
    std::string y;

    std::string repr() {
        return "Parameters { x = " + x + ", y = " + y + "}";
    }
};

double taxi_rate(double dist) {
    return 1.5 + 0.5 * dist;
}

int test_ops() {

    // agents involved

    const Agent<travel_state> me = { "me" };
    const Agent<travel_state> taxi = { "taxi" };

    // operators

    hop_operator<travel_state> walk = [&me](Task<travel_state> task) -> std::optional<Information<travel_state>> {
        if (task.info.state.loc[me.name] == task.info.params.x) {
            task.info.state.loc[me.name] = task.info.params.y;
            return task.info;
        } else {
            return std::nullopt;
        }
    };

    hop_operator<travel_state> call_taxi = [&taxi](Task<travel_state> task) -> std::optional<Information<travel_state>> {
        task.info.state.loc[taxi.name] = task.info.params.x;
        return task.info;
    };

    hop_operator<travel_state> ride_taxi = [&me, &taxi](Task<travel_state> task) -> std::optional<Information<travel_state>> {
        if ((task.info.state.loc[taxi.name] == task.info.params.x) && (task.info.state.loc[me.name] == task.info.params.x)) {
            task.info.state.loc[taxi.name] = task.info.params.y;
            task.info.state.loc[me.name] = task.info.params.y;
            task.info.state.owe = taxi_rate(task.info.state.dist[task.info.params.x][task.info.params.y]);
            return task.info;
        } else {
            return std::nullopt;
        }
    }; 

    hop_operator<travel_state> pay_driver = [](Task<travel_state> task) -> std::optional<Information<travel_state>> {
        if (task.info.state.cash > task.info.state.owe) {
            task.info.state.cash -= task.info.state.owe;
            task.info.state.owe = 0.0;
            return task.info;
        } else {
            return std::nullopt;
        }
    };

    hop_method<travel_state> travel_by_foot = [](Task<travel_state> task) -> std::optional<task_queue<travel_state>> {
        if (task.info.state.dist[task.info.params.x][task.info.params.y] <= 2.0) {  
            task_queue<travel_state> q = { { "walk", task.info } };
            return q;
        } else {
            return std::nullopt;
        }
    };

    hop_method<travel_state> travel_by_taxi = [](Task<travel_state> task) -> std::optional<task_queue<travel_state>> {
        if (task.info.state.cash > taxi_rate(task.info.state.dist[task.info.params.x][task.info.params.y])) {
            task_queue<travel_state> q;
            q.push_back({"call_taxi", task.info});
            q.push_back({"ride_taxi", task.info});
            q.push_back({"pay_driver", task.info});
            return q;
        } else {
            return std::nullopt;
        }
    }; 

    // solving the problem
    Universe<travel_state> universe;

    // add operators
    universe.add_operator("walk", walk);
    universe.add_operator("call_taxi", call_taxi);
    universe.add_operator("ride_taxi", ride_taxi);
    universe.add_operator("pay_driver", pay_driver);

    // add methods
    universe.add_methods("travel", {travel_by_foot, travel_by_taxi});

    // do it!
    travel_state the_state = { 
        .loc = {{me.name , "home"}, {taxi.name, "flerbs"}},
        .owe = 0.0,
        .dist = {
            {"home", {{"park" , 5.0}}}, 
            {"park", {{"home", 5.0}}}
        },
        .cash = 20.0
    };
    Parameters<travel_state> the_params = { .x = "home", .y = "park" };
    Information<travel_state> the_info = { .state = the_state, .params = the_params };
    Task<travel_state> do_travel = { .name = "travel", .info = the_info };
    task_queue<travel_state> tasks = { do_travel };
    auto result = universe.plan(tasks);
    
    if (result.has_value()) {
        std::cout << "solution found: " << display(result.value()) << std::endl;
    } else {
        std::cout << "solution not found" << std::endl;
    }

    return 0;
}

int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_ops()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}
