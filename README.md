# `hop9`

`hop9` is a hierarchical task network (HTN) planner written in C++. It's heavily inspired by Dana Nau's [`pyhop`](https://github.com/oubiwann/pyhop/blob/master/pyhop/hop.py) but is, you know, in C++ and also separates concerns between state, agent(s), and parameters. It does not have any dependencies aside from a C++20 compiler and a standard library implementation.

## Tests/Examples

Here: https://gitlab.com/drdewhurst/hop9/-/tree/master/test.

## Documentation

Here: https://davidrushingdewhurst.com/hop9/docs/index.html

## License

`hop9` is licensed under the GPL v3. 
If you would like a license exception please contact hop9@davidrushingdewhurst.com. 

Copyright David Rushing Dewhurst, 2023 - present.